//import("_include/global.js";

$(document).ready(function() {
  
  //import("_include/common.js";
  
  $('.slider-wrap').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: false,
    arrows: true,
    centerMode: false,
    autoplay: false,
    autoplaySpeed: 2000,
    nextArrow: '<i class="fa fa-arrow-right right" style="color:#ff0000"></i>',
    prevArrow: '<i class="fa fa-arrow-left left" style="color:#ff0000"></i>',
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 3,
        
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });
  if( $('#pagepiling').length > 0 ){
    $('#pagepiling').pagepiling({
      menu: null,
      direction: 'vertical',
      verticalCentered: true,
      css3: true,
      normalScrollElements: '.content-area',
      normalScrollElementTouchThreshold: 5,
      touchSensitivity: 5,
      keyboardScrolling: true,
      sectionSelector: '.section-select',
      animateAnchor: false,
      navigation: false
    });
  }

  $('.banner-slideshow').css({'width': viewportWidth})

  $('.banner-slideshow .slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    arrows: true,
    centerMode: false,
    appendArrows: '.banner-slideshow .controls',
    nextArrow: '<button class="slick-next slick-arrow" aria-label="Next" type="button">NEXT</button>',
    prevArrow: '<button class="slick-prev slick-arrow" aria-label="Prev" type="button">PREV</button>',
  });

  $('.mobile-menu .mobile-menu-trigger').click(function(){
    $('.menu-items .nav-items').toggleClass('active');
  })
});

//import("_include/outerclick.js";
if( $('.pp-scrollable').length){
  $('#backToTop').click(function(){
    $('.pp-scrollable').animate({scrollTop:0},500);
  });
  $('.pp-scrollable').scroll(function(){
    if($(this).scrollTop() > 40){
      $('#backToTop').fadeIn();
    }else{
      $('#backToTop').fadeOut();
    }
  });

} else {
  $(window).scroll(function(){
    if($(this).scrollTop() > 40){
      $('#backToTop').fadeIn();
    }else{
      $('#backToTop').fadeOut();
    }
  });
  $('#backToTop').click(function(){
    $('html,body').animate({scrollTop:0},500);
  });
}
$(document).ready(function() {
  $(".quick-link-select").select2({
    placeholder: "Quick Links",
    allowClear: false,
    // minimumResultsForSearch: -1
  });
});
$(".input-toggle-button,.mobile-search-trig").click(function(){
 $(".searchbar-dropdown").slideToggle();
});