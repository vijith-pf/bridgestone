$('.tab-items .tab-btn > a').click(function(e){
  $('.dropdown-options-wrap, .dropdown-wrapper').removeClass('active');
  $(this).parent().siblings().removeClass('active');
  $(this).parent().toggleClass('active');
  e.preventDefault();
});

$('.tab-items-sub .btn').click(function(e){
  var selectedTab = $(this).attr('href');
  //$('.dropdown-options-wrap').toggleClass('active');
  $('.dropdown-wrapper'+selectedTab).siblings().removeClass('active');
  $('.dropdown-wrapper'+selectedTab).toggleClass('active');
  
  $(this).parent().siblings().find('.btn').removeClass('active');
  $(this).toggleClass('active');

  if( $('.dropdown-wrapper.active').length > 0 ){
    $('.dropdown-options-wrap').addClass('active');
  } else {
    $('.dropdown-options-wrap').removeClass('active');
  }

  e.preventDefault();
});