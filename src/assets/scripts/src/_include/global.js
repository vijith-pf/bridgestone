var windowHeight = $(window).height();
var viewportWidth = $(window).width();

function bodyFreezeScroll() {
  window.addEventListener('scroll', noscroll);
  $('body').css({'overflow': 'hidden', 'max-height': windowHeight});
  $('body').addClass('scroll-disabled');
}

function bodyUnfreezeScroll() {
  window.removeEventListener('scroll', noscroll);
  $('body').css({'overflow': 'auto', 'max-height': 'inherit'});
  $('body').removeClass('scroll-disabled');
}