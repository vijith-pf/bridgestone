$('a[data-goto-href]').click(function(e){
	var scrollto = $(this).attr('href');
	$('body').scrollTo(scrollto, 600);
	e.preventDefault();
});