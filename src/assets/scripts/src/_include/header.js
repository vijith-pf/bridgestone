$('.search-menu a').click(function(e){
  $('header').toggleClass('search-active');
  $(this).parent().toggleClass('active');
  e.preventDefault();
});

$('header .navigation .mobile-nav-trigger').click(function(e){
  $('.mobile-menu-popup').addClass('active');
  e.preventDefault();
});

$('header .mobile-menu-popup .close-btn').click(function(e){
  $('.mobile-menu-popup').removeClass('active');
  e.preventDefault();
});